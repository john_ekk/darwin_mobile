starter.controller('adminsCtrl', function($scope, $http) {
function loadAdmin(callback) {
    var administration = [];
    var asset = [];

    $http.get('http://darwin.cloudapp.net/api/user').success(function(response) {
      angular.forEach(response, function(child) {
        var user = child;
        if (user.statut == 'ADM')
          administration.push(user);
        else
          asset.push(user);
      });

      callback(administration, asset);
    });
  }
  loadAdmin(function(administration, asset) {
      $scope.administration = administration;
      $scope.asset = asset;
      
  });

  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleAdmin = function(admin) {
    if ($scope.isAdminShown(admin)) {
      $scope.shownAdmin = null;
    } else {
      $scope.shownAdmin = admin;
    }
  };
  $scope.isAdminShown = function(admin) {
    return $scope.shownAdmin === admin;
  };
  
});