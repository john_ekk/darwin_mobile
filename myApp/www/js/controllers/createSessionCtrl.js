starter.controller('createSessionCtrl', function($scope, $state, $http) {
	$scope.data = {};
    $scope.createSession = function() {
        var data = {
                name: this.nameSession,
                date: this.dateSession,
                present: 0,
                all: 0,
            };

       	$http.post("http://darwin.cloudapp.net/api/session", data).success(function() {
            $state.go('app.home', {}, {reload: true});
        }).error(function (data, status, header, config) {
               	console.log("Data: " + data +
                    "<hr />status: " + status +
                    "<hr />headers: " + header +
                    "<hr />config: " + config);
            });

    }
})