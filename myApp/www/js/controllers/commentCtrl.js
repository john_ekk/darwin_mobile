starter.controller('commentCtrl', function($scope, $state, $http) {
	var id 	  = $state.params.id;
	var type  = $state.params.type;
	var date  = new Date();
	var month = date.getMonth() + 1;

	if (month.toString().length == 1)
		month = '0' + month;
	
    $scope.addComment = function() {
    	
        var data = {
                author: this.author,
                date: 	date.getDate() + '-' + month + '-' + date.getFullYear(),
                title: 	this.commTitle,
                text: 	this.comment,
            };

        if ( type == 'session') {
	       	$http.post("http://darwin.cloudapp.net/api/session/" + $state.params.id + "/comment", data).success(function() {
	       		$state.go('app.session', {session_id: $state.params.id}, {reload: true});
	        }).error(function (data, status, header, config) {
	               	console.log("Data: " + data +
	                    "<hr />status: " + status +
	                    "<hr />headers: " + header +
	                    "<hr />config: " + config);
	            });
    	}

        else if ( type == 'student') {
	       	$http.post("http://darwin.cloudapp.net/api/student/" + $state.params.id + "/comment", data).success(function() {
	       		$state.go('app.student', {student_id: $state.params.id}, {reload: true});
	        }).error(function (data, status, header, config) {
	               	console.log("Data: " + data +
	                    "<hr />status: " + status +
	                    "<hr />headers: " + header +
	                    "<hr />config: " + config);
	            });
    	}

    }
});