starter.controller('ExCtrl', function($scope, $state, $http) {
  console.log($state.params.ex_id);

  function loadEx(callback) {
      var ex = {};

      $http.get('http://darwin.cloudapp.net/api/exo/' + $state.params.ex_id).success(function(response) {
          ex = response;
          console.log(ex)
          console.log(ex.nom)

          callback(ex);
      });
    }

    loadEx(function(ex) {
      $scope.ex = ex;
    });

})