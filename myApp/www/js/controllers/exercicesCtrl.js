starter.controller('exoCtrl', function($scope, $http) {
function loadExo(callback) {
    var exo = [];
    var exoC = [];
    var exoShell = [];

    $http.get('http://darwin.cloudapp.net/api/exo').success(function(response) {
        angular.forEach(response, function(child) {
            var ex = child;
            if (ex.type == 'C' || ex.type == 'c')
              exoC.push(ex);
            else if(ex.type == 'Shell' || ex.type == 'shell')
              exoShell.push(ex);
            else
              exo.push(ex);
        });
        
        callback(exoC, exoShell);
    });
  }
  loadExo(function(exoC, exoShell) {
      $scope.exoC = exoC;
      $scope.exoShell = exoShell;
      
  });

  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleExo = function(exo) {
    if ($scope.isExoShown(exo)) {
      $scope.shownExo = null;
    } else {
      $scope.shownExo = exo;
    }
  };
  $scope.isExoShown = function(exo) {
    return $scope.shownExo === exo;
  };

  $scope.deleteEx = function(exo) {
    $http.delete('http://darwin.cloudapp.net/api/exo/' + exo).success(function(response) {
        location.reload();
    });    
  };
  
});