starter.controller('adminDetailsCtrl', function($scope, $http, $state) {
  function loadAdminDetails(callback) {
    var adminDetails = {};

    $http.get('http://darwin.cloudapp.net/api/user/' + $state.params.admin_id).success(function(response) {
          adminDetails = response;     
          callback(adminDetails);
      });
  }

  loadAdminDetails(function(adminDetails) {
    $scope.admin = adminDetails;
  });

});