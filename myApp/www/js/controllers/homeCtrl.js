starter.controller('Home', function($scope, $state, $http) {
  function loadSessions(callback) {
    console.log('load');
    var sessions = [];
    $http.get('http://darwin.cloudapp.net/api/session').success(function(response) {
        angular.forEach(response, function(child) {
            var session = child;
            sessions.push(session);
        });
        console.log(sessions);
        callback(sessions);
    });
  }
  loadSessions(function(sessions) {
      $scope.sessions = sessions;
  });
  
  $scope.toggleSession = function(session) {
    if ($scope.isSessionShown(session)) {
      $scope.shownSession = null;
    } else {
      $scope.shownSession = session;
    }
  };
  $scope.isSessionShown = function(session) {
    return $scope.shownSession === session;
  };

  $scope.delete = function(session) {
    $http.delete('http://darwin.cloudapp.net/api/session/' + session).success(function(response) {
        location.reload();
    });    
  };

  $scope.createSession = function() {
    $state.go('app.createSession');
  }; 
})