starter.controller('SessionCtrl', function($scope, $state, $http) {
	console.log($state.params.session_id);

  var session = {};
	function loadSession(callback) {
    	

    	$http.get('http://darwin.cloudapp.net/api/session/' + $state.params.session_id).success(function(response) {
        	session = response;
       
        	callback(session);
    	});
  	}

  	loadSession(function(session) {
    	$scope.session = session;
  	});



  function loadStudents(callback) {
    var students = [];

    $http.get('http://darwin.cloudapp.net/api/student').success(function(response) {
        angular.forEach(response, function(child) {
            var student = child;
            if (student.session == session.name)
            	students.push(student);
        });
        callback(students);
    });
  }
  loadStudents(function(students) {
    console.log(students);
      $scope.students = students;
  });

  $scope.toggleStudents = function(students) {
    if ($scope.isStudentsShown(students)) {
      $scope.shownStudents = null;
    } else {
      $scope.shownStudents = students;
    }
  };
  $scope.isStudentsShown = function(students) {
    return $scope.shownStudents === students;
  };


  $scope.toggleComments = function(comments) {
    if ($scope.isCommentsShown(comments)) {
      $scope.shownComments = null;
    } else {
      $scope.shownComments = comments;
    }
  };
  $scope.isCommentsShown = function(comments) {
    return $scope.shownComments === comments;
  };

  $scope.addComment = function(type, id) {
    $state.go('app.comment', {type: type, id: id});
  };  

})