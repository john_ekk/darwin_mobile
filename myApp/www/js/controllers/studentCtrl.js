starter.controller('StudentCtrl', function($scope, $state, $http) {
  console.log($state.params.student_id);
  var student = {};

  function loadStudent(callback) {
      $http.get('http://darwin.cloudapp.net/api/student/' + $state.params.student_id).success(function(response) {
          student = response;

          callback(student);
      });
    }

    loadStudent(function(student) {
      console.log(student);
      console.log(student.comment);
      $scope.student = student;
    });

  function loadExo(callback) {
      var exo = [];

      $http.get('http://darwin.cloudapp.net/api/exo/').success(function(response) {
        angular.forEach(response, function(child) {
            var ex = child;
            if (ex.version == student.version)
              exo.push(ex);
        });

        callback(exo);
      });
    }

    loadExo(function(exo) {
      console.log(exo);
      $scope.exo = exo;
    });

  $scope.addComment = function(type, id) {
    $state.go('app.comment', {type: type, id: id});
  }; 

  $scope.toggleExo = function(exo) {
    if ($scope.isExoShown(exo)) {
      $scope.shownExo = null;
    } else {
      $scope.shownExo = exo;
    }
  };

  $scope.isExoShown = function(exo) {
    return $scope.shownExo === exo;
  };

  $scope.toggleComments = function(comments) {
    if ($scope.isCommentsShown(comments)) {
      $scope.shownComments = null;
    } else {
      $scope.shownComments = comments;
    }
  };
  $scope.isCommentsShown = function(comments) {
    return $scope.shownComments === comments;
  };

})