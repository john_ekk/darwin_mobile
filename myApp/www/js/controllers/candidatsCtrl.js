starter.controller('candidatsCtrl', function($scope, $http) {
  	function loadSessions(callback) {
	    var nameSessions = [];

	    $http.get('http://darwin.cloudapp.net/api/session').success(function(response) {
		    angular.forEach(response, function(child) {
		        var nameSession = child;
		        nameSessions.push(nameSession.name);
		    });

	        callback(nameSessions);
	    });
  	};

    loadSessions(function(nameSessions) {
       console.log(nameSessions);
      $scope.nameSessions = nameSessions;
    });

    $scope.selectSession = function (session) {
    	console.log(this.sessionSelected);
	    var students = [];
	    var url = 'http://darwin.cloudapp.net/api/student';

	    if ( session != "all" )
	    	url = url + '/session/' + this.sessionSelected;
	    console.log(url);

	    $http.get(url).success(function(response) {
		    angular.forEach(response, function(child) {
		        var student = child;

		        if (student.photo == null)
		        	student.photo = "http://darwin.cloudapp.net/img/user.png";

		        students.push(student);
		    });
		    console.log(students);
	    });
        $scope.students = students;
  	};

  	$scope.selectSession("all");


  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleCandidat = function(candidat) {
    if ($scope.isCandidatShown(candidat)) {
      $scope.shownCandidat = null;
    } else {
        $scope.shownCandidat = candidat;
      }
  };
  
  $scope.isCandidatShown = function(candidat) {
    return $scope.shownCandidat === candidat;
  };
});