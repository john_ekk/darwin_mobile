// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var starter = angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

  .config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl'
    })
  
    .state('app.search', {
      url: "/search",
      views: {
        'menuContent' :{
          templateUrl: "templates/search.html"
        }
      }
    })

    .state('app.home', {
      cache: false,
      url: "/home",
      views: {
        'menuContent' :{
          templateUrl: "templates/home.html"
        }
      }
    })

    .state('app.candidats', {
      url: "/candidats",
      views: {
        'menuContent' :{
          templateUrl: "templates/candidats.html"
        }
      }
    })

    .state('app.exercices', {
      url: "/exercices",
      views: {
        'menuContent' :{
          templateUrl: "templates/exercices.html"
        }
      }
    })

    .state('app.exercice', {
      url: "/exercice/:ex_id",
      views: {
        'menuContent' :{
          templateUrl: "templates/exercice.html"
        }
      }
    })
    
    .state('app.admin', {
      url: "/administration",
      views: {
        'menuContent' :{
          templateUrl: "templates/admins.html",
        }
      }
    })

    .state('app.adminDetails', {
      url: "/adminDetails/:admin_id",
      views: {
        'menuContent' :{
          templateUrl: "templates/adminDetails.html",
        }
      }
    })
  
  .state('app.login-into-menucontent', {
    url: "/login-into-menucontent",
    views: {
      'menuContent' :{
        templateUrl: "templates/login.html",
        controller: 'LoginCtrl'
      }
    }
  })
    
  .state('login', {
      url: "/login",
      templateUrl: "templates/login.html",
      controller: 'LoginCtrl'
    })

  .state('app.createSession', {
      url: "/createSession",
      views: {
        'menuContent' :{
          templateUrl: "templates/createSession.html"
        }
      }
  })

  .state('app.session', {
      cache: false,
      url: "/session/:session_id",
      views: {
        'menuContent' :{
          templateUrl: "templates/session.html"
        }
      }
  })

    .state('app.comment', {
      url: "/comment/:type/:id",
      views: {
        'menuContent' :{
          templateUrl: "templates/addComment.html"
        }
      }
  })

  .state('app.student', {
      cache: false,
      url: "/student/:student_id",
      views: {
        'menuContent' :{
          templateUrl: "templates/student.html"
        }
      }
  })

  .state('app.modifyStudent', {
      url: "/modifyStudent/:student_id",
      views: {
        'menuContent' :{
          templateUrl: "templates/modifyStudent.html"
        }
      }
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

})

.controller('AppCtrl', function($scope) {
})

